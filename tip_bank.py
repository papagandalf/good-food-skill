# -*- coding: utf-8 -*-

FOOD_WASTE_TIPS = [
    'One way to manage food waste is to make sure you measure your food to get the right portions.',
    'One way to manage food waste is to plan your meals so you are less likely to have leftovers.',
    'One way to manage food waste is to store your food well so it lasts as long as possible and make sure you '
    'understand food date labels.',
    'One way to manage food waste is to keep a food waste diary for a week to help you understand how much and what '
    'foods you waste at home.',
    'One way to manage food waste is to make a meal plan and donate extra food to soup kitchens.',
    'One way to manage food waste is to compost kitchen and garden scrap.',
    'One way to manage food waste is to donate waste for animal feed.',
    'Did you know that over one-third of the food produced across the globe is either discarded or wasted? This food '
    'waste holds a huge untapped potential for generating energy, commonly referred to as waste-to-energy systems.',
    'Did you know that anaerobic digestion uses microorganisms to degrade the organic matter in the food waste to '
    'produce methane, that can be used to generate electricity, fuel for transportation, and heat?',
    'Did you know that biogas is a renewable and sustainable source of energy developed from organic matter such as '
    'waste residues of vegetables and fruits, scrap timber, and forest debris?',
    'There are many benefits in composting. For example, recycling reduces the volume of the periodically collected '
    'garbage, and in turn, the amount of garbage in the landfills. It directly contributes to lowering the methane '
    'emissions.',
    'There are many benefits in composting one of them is that it might earn you money if you have someone to buy the '
    'end product of your compost.',
    'There are many benefits in composting one of them is that it saves you money from purchasing soil and '
    'commercially made fertilisers for your garden.',
    'There are many benefits in composting one of them is that if you use it on your own vegetable garden, it also '
    'saves you money from buying less food.',
    'There are many benefits in composting one of them is that saves the food industry energy and resources, and same '
    'goes for the garbage collecting industry.',
    'There are many benefits in composting one of them is that it improves your garden\'s health.',
    'There are many benefits in composting one of them is that it reduces water use in your garden.'
]

RECYCLING_WASTE_TIPS = [
    'One way to reduce the single-use plastic in your life is to cook from raw ingredients when you have the time and '
    'inclination. this often results in less packaging, and less food waste too.',
    'One way to reduce the single-use plastic in your life  is to cut back on unnecessary packaging by trying out '
    'scoop shops, or by buying from greengrocers and delis who use paper bags. You could also look out for zero-waste '
    'shops. ',
    'One way to reduce the single-use plastic in your life  is to try alternative packaging systems and product '
    'refills via home deliveries. Milk floats are having a moment with some consumers shunning plastic in favour of '
    'bottled milk. ',
    'One way to reduce the single-use plastic in your life  is to look for takeaway lunch places that serve food in '
    'cardboard boxes rather than plastic (or just bring in last night\'s leftovers for lunch at work.',
    'One way to reduce the single-use plastic in your life  is to buy loose leaf tea. Many people don\'t realise that '
    'some teabags contain plastic.',
    'One way to reduce the single-use plastic in your life  is to support supermarkets\' endeavors to reduce plastic. '
    'For example, Iceland has pledged a significant reduction in plastic use and Lidl now has plastic-free rice and '
    'couscous products. By supporting these measures, you can encourage other food sellers to follow suit.',
    'One way to reduce the plastics you already have is to get a keep coffee cup. The government has said that '
    'throwaway cups should be prohibited altogether by 2023, part of its 25-year environmental plan, which addresses '
    'plastic pollution.',
    'One way to reduce the plastics you already have is to top up your reusable water bottle. Bristol and now '
    'other locations, have  a refill initiative with stickers in restaurants and venue windows indicating '
    're-fill stations, where you can fill up your bottle with tap water.',
    'One way to reduce the plastics you already have is  to not be shy to decline a plastic straw when you\'re out '
    'drinking. Or get a reusable one. Standard bar plastic straws can\'t be recycled and can take 200 years to break '
    'down. And spread the word. the Scottish village of Ullapool had a successful #NaeStrawAtAw campaign, '
    'led by children, leading to plastic straws being scrapped from all venues.',
    'One way to reduce the plastics you already have is to use old food tubs for storing leftover food and look for '
    'biodegradable cling film.',
    'One way to recycle plastics and other materials is to look for products with recyclable packaging. For example, '
    'avoid polystyrene pizza discs and look for cardboard instead.',
    'Check on the website of your local waste company to be sure that you are recycling everything that can be '
    'recycled. For example yogurt pots, margarine tubs and Tetra Pak juice containers. Many areas will not collect '
    'black plastic, but there might be some surprises. For example, used kitchen foil can be recycled through many '
    'services.',
    'Recycle carrier bags if you have had to use them (many supermarkets have carrier bag recycling facilities).',
    'Try the MSC plastic challenge with your family to monitor your non-recyclable and recyclable plastic use.',
    'If you think an item has used excess packaging, complain about it via the supermarket or manufacturers. '
    'Ask supermarkets for refillable product containers and keep an eye out for alternative packaging. '
    'Consumer pressure will encourage supermarkets to up their game.',
    'One way to cut down on plastic waste is to take your own bags and reusable boxes with you when you go shopping.',
    'One way to cut down on plastic waste is to shop in traditional stores that pack purchases more lightly.',
    'One way to cut down on plastic waste is to buy dry foods, minimally wrapped, from wholefood stores and then '
    'decant them into storage containers.',
    'One way to cut down on plastic waste is to buy milk in returnable glass bottles from farm shop vending machines.',
    'One way to cut down on plastic waste is to buy fewer packaged, processed foods.',
    'One way to cut down on plastic waste is to sign up for a veg box, which tends to be lightly packaged.',
    'One way to cut down on plastic waste is to avoid individually wrapped portions if possible.',
    'One way to cut down on plastic waste is to use loose tea leaves and coffee rather than teabags and coffee pods.',
    'One way to cut down on plastic waste is to buy products with a single type of recyclable packaging when you can.',
    'One way to cut down on plastic waste is to invest in a steel water bottle rather than regularly buying plastic '
    'bottles.',
]

USE_BY_TIPS = [
    'Expires On label. You will only see this on infant formula or some baby foods. Always use the product before the '
    'expiration date.',
    'Sell By label.Tells the store how long to display the product for sale. You should buy the product before the '
    'date expires, but this is not a safety date and you can use the product after the sell-by date.',
    'Best If Used By (or Before) label. Date recommended for best flavor or quality. It is not a purchase or safety '
    'date.',
    'Use By label. The last date recommended for the use of the product while at peak quality. Adhere to this date '
    'for meat and eggs.',
    'Born On label. Most commonly used to date beer, this refers to the date the beer was bottled.',
    'Guaranteed Fresh label. Usually referring to bakery items and when to eat them by for peak freshness.',
    'Closed or Coded Dating. Usually on canned or nonperishable foods, this is the trickiest and most confusing one, '
    'as it can be a date or the date can be coded by the manufacturer.'
]