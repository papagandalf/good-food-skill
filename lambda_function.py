# -*- coding: utf-8 -*-

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_model.dialog import delegate_directive
from ask_sdk_model.ui import AskForPermissionsConsentCard

from ask_sdk_model.ui import SimpleCard

import logic

from six import PY2
try:
    from HTMLParser import HTMLParser
except ImportError:
    from html.parser import HTMLParser


def contains_slot(slot, slots):
    print(slot)
    if slot in slots and slots[slot] is not None and getattr(slots[slot], 'value', None) is not None:
        return True
    else:
        return False


# convert SSML to card text
# This is for automatic conversion of ssml to text content on simple card
class SSMLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.full_str_list = []
        if not PY2:
            self.strict = False
            self.convert_charrefs = True

    def handle_data(self, d):
        self.full_str_list.append(d)

    def get_data(self):
        return ''.join(self.full_str_list)


skill_name = "good food"
help_text = "Ask anything regarding food waste and recycling."

color_slot = {'slot_name': 'Color', 'key': 'COLOR'}
food_item_slot = {'slot_name': 'food_item', 'key': 'FOOD'}
safe_to_eat = {'slot_name': 'safe_to_eat', 'key': 'SAFE_TO_EAT'}
feed_more_than_one = {'slot_name': 'feed_more_than_one', 'key': 'FEED_MORE_THAN_ONE'}
feel_like_cooking = {'slot_name': 'feel_like_cooking', 'key': 'FEEL_LIKE_COOKING'}

permissions = ["read::alexa:device:all:address"]

sb = SkillBuilder()


@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):
    speech = "Welcome to the good food skill. You can ask me anything about excess food, food waste, recycling and " \
             "redistributing food. Make the best out of your food."
    reprompt = "You can ask me anything about excess food, food waste, recycling and redistributing food. Make the " \
               "best out of your food."

    handler_input.response_builder.speak(speech).ask(reprompt)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):
    handler_input.response_builder.speak(help_text).ask(help_text)
    return handler_input.response_builder.response


@sb.request_handler(
    can_handle_func=lambda input:
        is_intent_name("AMAZON.CancelIntent")(input) or
        is_intent_name("AMAZON.StopIntent")(input))
def cancel_and_stop_intent_handler(handler_input):
    # Single handler for Cancel and Stop Intent
    speech_text = "Goodbye!"

    return handler_input.response_builder.speak(speech_text).response


@sb.request_handler(can_handle_func=is_request_type("SessionEndedRequest"))
def session_ended_request_handler(handler_input):
    # Handler for Session End
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("recycleFoodPlaceIntent"))
def recycle_food_intent(handler_input):
    speech, status = logic.get_food_recycling_location(handler_input)
    if status == 'no_address_permissions':
        handler_input.response_builder.speak(speech)
        handler_input.response_builder.set_card(
            AskForPermissionsConsentCard(permissions=permissions))
    else:
        handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("recyclePackagingSpecificIntent"))
def recycle_packaging_specific_intent(handler_input):
    speech, status = logic.get_packaging_recyclinc_location(handler_input)
    if status == 'no_address_permissions':
        handler_input.response_builder.speak(speech)
        handler_input.response_builder.set_card(
            AskForPermissionsConsentCard(permissions=permissions))
    else:
        handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("recyclePackagingPlaceIntent"))
def recycle_packaging_intent(handler_input):
    speech, status = logic.get_packaging_recyclinc_location(handler_input)
    if status == 'no_address_permissions':
        handler_input.response_builder.speak(speech)
        handler_input.response_builder.set_card(
            AskForPermissionsConsentCard(permissions=permissions))
    else:
        handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("foodWasteTipsIntent"))
def food_waste_tips(handler_input):
    tip = logic.get_food_recycling_tips()
    speech, reprompt = tip, tip
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("packagingRecyclingTipsIntent"))
def package_recycling_tips(handler_input):
    tip = logic.get_package_recycling_tips()
    speech, reprompt = tip, tip
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("foodBankPlaceIntent"))
def food_bank_place_intent(handler_input):
    speech, status = logic.food_bank_position(handler_input)
    if status == 'no_address_permissions':
        handler_input.response_builder.speak(speech)
        handler_input.response_builder.set_card(
            AskForPermissionsConsentCard(permissions=permissions))
    else:
        handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("foodBankGiveSpecificIntent"))
def food_bank_specific_place_intent(handler_input):
    speech, status = logic.food_bank_position(handler_input)
    if status == 'no_address_permissions':
        handler_input.response_builder.speak(speech)
        handler_input.response_builder.set_card(
            AskForPermissionsConsentCard(permissions=permissions))
    else:
        handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("useByTipsIntent"))
def use_by_tips(handler_input):
    tip = logic.get_use_by_tip()
    speech, reprompt = tip, tip
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("iDoRecyclePackagingIntent"))
def i_do_recycle_packaging(handler_input):
    req_envelope = handler_input.request_envelope
    ok = logic.save_recycling(req_envelope.context.system.user.user_id, 'package')
    speech = ok
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("iDontRecyclePackagingIntent"))
def i_do_recycle_packaging(handler_input):
    req_envelope = handler_input.request_envelope
    ok = logic.save_recycling(req_envelope.context.system.user.user_id, 'no_package')
    speech = ok
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("iDoFoodWasteIntent"))
def i_do_recycle_packaging(handler_input):
    req_envelope = handler_input.request_envelope
    ok = logic.save_recycling(req_envelope.context.system.user.user_id, 'food_waste')
    speech = ok
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("expirationDatesIntent"))
def expiration_dates(handler_input):
    speech = 'Expiration date can mean lots of things. For example, Best Before indicates the date recommended for ' \
             'best flavor or quality. It is not a purchase or ssafety date. Use By Date is the last date recommended ' \
             'for the use of the product while at peak quality. Stick to this date for meat and eggs.'
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("otherCitiesIntent"))
def expiration_dates(handler_input):
    speech = 'The good food skill uses open data on waste bin availability and locations. For now it is only ' \
             'available for Edinburgh, UK but be patient and in the future my smart developer will add more cities.'
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("iDontDoFoodWasteIntent"))
def i_do_recycle_packaging(handler_input):
    req_envelope = handler_input.request_envelope
    ok = logic.save_recycling(req_envelope.context.system.user.user_id, 'no_food_waste')
    speech = ok
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


def route(slots, handler_input):
    if slots[safe_to_eat['slot_name']].value == 'no':
        # waste
        speech = 'Never consume something that is not safe to eat. If you need tips on expiration dates, ask me. '
        location_speech, location_status = logic.dispose_food(handler_input)
        speech += location_speech
        if location_status != 'ok':
            speech += ' Knowing your location, I can recommend nearby places.'
    elif slots[feed_more_than_one['slot_name']].value == 'yes' and slots[feel_like_cooking['slot_name']].value == 'no':
        # consider food bank
        speech = 'Have you thought about giving excess food to local charities that need it? '
        location_speech, location_status = logic.food_bank_position(handler_input)
        speech += location_speech
        if location_status != 'ok':
            speech += ' Knowing your location, I can recommend nearby places.'
    elif slots[feel_like_cooking['slot_name']].value == 'yes':
        speech = 'You can make some delicious recipes with leftovers. ' \
                 'Ask Amazon Alexa for recipes with your ingredients. '
        location_status = 'ok'
    else:
        location_speech, location_status = logic.dispose_food(handler_input)
        speech = location_speech
    return speech, location_status


@sb.request_handler(can_handle_func=is_intent_name("getOptionsIntent"))
def use_by_tips(handler_input):
    slots = handler_input.request_envelope.request.intent.slots

    if contains_slot(safe_to_eat['slot_name'], slots) and contains_slot(feed_more_than_one['slot_name'], slots) \
            and contains_slot(feel_like_cooking['slot_name'], slots):
        speech, location_status = route(slots, handler_input)
        if location_status == 'no_address_permissions':
            handler_input.response_builder.speak(speech)
            handler_input.response_builder.set_card(
                AskForPermissionsConsentCard(permissions=permissions))
        else:
            handler_input.response_builder.speak(speech)
    elif contains_slot(safe_to_eat['slot_name'], slots) and slots[safe_to_eat['slot_name']].value == 'no':
        speech, location_status = route(slots, handler_input)
        if location_status == 'no_address_permissions':
            handler_input.response_builder.speak(speech)
            handler_input.response_builder.set_card(
                AskForPermissionsConsentCard(permissions=permissions))
        else:
            handler_input.response_builder.speak(speech)
    else:
        handler_input.response_builder.add_directive(delegate_directive.DelegateDirective())
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.FallbackIntent"))
def fallback_handler(handler_input):
    # AMAZON.FallbackIntent is only available in en-US locale.
    speech = 'The good food skill can\'t help you with that, I\'m afraid.'
    reprompt = 'You can ask anything about food waste and recycling.'
    handler_input.response_builder.speak(speech)
    return handler_input.response_builder.response


def convert_speech_to_text(ssml_speech):
    # convert ssml speech to text, by removing html tags
    s = SSMLStripper()
    s.feed(ssml_speech)
    return s.get_data()


@sb.global_response_interceptor()
def add_card(handler_input, response):
    # Add a card by translating ssml text to card content
    if getattr(response, 'output_speech') and response.output_speech is not None:
        if getattr(response.output_speech, 'ssml') and response.output_speech.ssml != '<speak>Please enable Location permissions in the Amazon Alexa app.</speak>':
            response.card = SimpleCard(
                title=skill_name,
                content=convert_speech_to_text(response.output_speech.ssml))


@sb.global_response_interceptor()
def log_response(handler_input, response):
    # Log response from alexa service
    print('Alexa Response: {}\n'.format(response))


@sb.global_request_interceptor()
def log_request(handler_input):
    # Log request to alexa service
    print('Alexa Request: {}\n'.format(handler_input.request_envelope.request))


@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):
    # Catch all exception handler, log exception and
    # respond with custom message
    print('Encountered following exception: {}'.format(exception))

    speech = 'Sorry, there was some problem. Please try again.'
    handler_input.response_builder.speak(speech).ask(speech)

    return handler_input.response_builder.response


# Handler to be provided in lambda console.
handler = sb.lambda_handler()
