import os
import MySQLdb


def connect_to_db():
    db = MySQLdb.connect(host=os.environ['db_host'], user=os.environ['db_user'], passwd=os.environ['db_pass'],
                         db=os.environ['db_name'])
    return db


def do_i_recycle_packaging(user_id):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("SELECT * FROM user_options WHERE user_id='{}' and recycle_packaging=True".format(user_id))
    if mycursor.rowcount > 0:
        to_return = True
    else:
        to_return = False
    db.close()
    return to_return


def do_i_recycle_food(user_id):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("SELECT * FROM user_options WHERE user_id='{}' and recycle_food=True".format(user_id))
    if mycursor.rowcount > 0:
        to_return = True
    else:
        to_return = False
    db.close()
    return to_return


def save_recycle_packaging(user_id, do_i_recycle):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("SELECT * FROM user_options WHERE user_id='{}'".format(user_id))
    print(mycursor.rowcount)
    if mycursor.rowcount > 0:
        mycursor.execute("UPDATE user_options SET recycle_packaging={} WHERE user_id='{}'".format(str(do_i_recycle), user_id))
    else:
        mycursor.execute("INSERT INTO user_options (recycle_packaging, user_id) VALUES({}, '{}')".format(str(do_i_recycle), user_id))
    db.commit()
    db.close()


def save_recycle_food(user_id, do_i_recycle):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("SELECT * FROM user_options WHERE user_id='{}'".format(user_id))
    if mycursor.rowcount > 0:
        mycursor.execute("UPDATE user_options SET recycle_food={} WHERE user_id='{}'".format(str(do_i_recycle), user_id))
    else:
        mycursor.execute("INSERT INTO user_options (recycle_food, user_id) VALUES({}, '{}')".format(str(do_i_recycle), user_id))
    db.commit()
    db.close()


def find_closest_recycling_bin(point):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("""SELECT 
      CONCAT(P.site_name, ' ', P. full_name),
      ST_AsText(P.point),
      ST_DISTANCE(ST_GeomFromText('POINT({} {})'), P.point) AS dist 
      FROM recycling P
      ORDER BY dist
      LIMIT 1
      """.format(point[0], point[1]))
    myresult = mycursor.fetchall()

    result = ''
    for x in myresult:
        result = x
    db.close()
    return result


def find_closest_food_waste_bin(point):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("""SELECT 
      full_name,
      ST_AsText(P.point),
      ST_DISTANCE(ST_GeomFromText('POINT({} {})'), P.point) AS dist 
      FROM food_waste P
      ORDER BY dist
      LIMIT 1
      """.format(point[0], point[1]))
    myresult = mycursor.fetchall()

    result = ''
    for x in myresult:
        result = x
    db.close()
    return result


def find_closest_food_bank(point):
    db = connect_to_db()
    mycursor = db.cursor()
    mycursor.execute("""SELECT 
      CONCAT(P.full_name, ' in ', P. address),
      ST_AsText(P.point),
      ST_DISTANCE(ST_GeomFromText('POINT({} {})'), P.point) AS dist 
      FROM food_bank P
      ORDER BY dist
      LIMIT 1
      """.format(point[0], point[1]))
    myresult = mycursor.fetchall()

    result = ''
    for x in myresult:
        result = x
    db.close()
    return result
