## outline

This Amazon Alexa skill is my submission to the [Alexa Skills Challenge: Tech for Good](https://alexatechforgood.devpost.com/). It is called good food.

In a nutshell, it promotes sustainable living by providing advice and tips regarding food waste disposal, food recycling and redistribution.

## details

Some example questions that you can ask the skill:
* "Alexa, tell good food that I have some extra salad".
* "Alexa, ask good food where the closest package recycling bin is."  
* "Alexa, ask good food where the closest food waste bin is."
* "Alexa, ask good food about recycling."
* "Alexa, ask good food about food waste recycling."
* "Alexa, ask good food what BB is."
* "Alexa, ask good food what best before is."
* "Alexa, ask good food where the closes food bank is."
* "Alexa, tell good food that I recycle food waste." (Persistently saves user's preference, so that they are not presented with locations of bins that they already use daily).

Good food will provide you with advice on how to dispose, recycle or redistribute food in one of the following ways: dispose food waste to close-by bins, "donate" to close-by charities or food banks, or cook something with leftovers.
Moreover, it can provide you with locations of the closest package recycling and food waste bins and lots of tips regarding food waste. 

## data

For now, and for the needs of the submission to the challenge, the good food skill includes data about bins in Edinburgh, UK. The reason for that is that I live in Edinburgh. 
* Food waste bin data was taken from the [Edinburgh communal bins map](https://edinburghcouncil.maps.arcgis.com/apps/webappviewer/index.html?id=bd24cbb75a364134ad027659eb3b2033).
* Mixed recycling bin data was taken from [Edinburgh open data portal](https://data.edinburghopendata.info/dataset/recycling-points).

This kind of data is relatively easy to find for other cities, so it easy to extend good food's functionality to more cities.

## amazon alexa implementation details

The implementation is in Python, uses the [Alexa Skills Kit SDK for Python](https://github.com/alexa-labs/alexa-skills-kit-sdk-for-python). For some of the intents, it uses Dialog Management. Also, it persistently stores the users recycling habits, so that it personalises the ouput for some responses.
Moreover, all the interactions have corresponding cards that can be viewed from the Amazon Alex app and/or monitor-enabled devices as Echo Spot or Echo Show.