## privacy policy

### about
This privacy policy discloses the privacy practices for the Amazon Alexa skill "good food "
submitted to the [Alexa SKills Challenge: Tech for good](https://alexatechforgood.devpost.com).
 

This privacy policy applies solely to information collected by this software. 

It will notify you of the following: 

* what personally identifiable information is collected from you through 
the use of the skill, how it is used and with whom it may be shared. 
* what choices are available to you regarding the use of your data. 
* the security procedures in place to protect the misuse of your information. 
* how you can correct any inaccuracies in the information. 
 

### information collection, use, and sharing

The only information this skill can collect and store is whether a user recycles or not. This 
 information is collected voluntarily and only by the act of the user explicitly
 using this feature of the skill. No personal or identifiable information are stored or shared 
 via the use of this skill.

The skill needs the user's permission to access their Amazon Echo device's location, with the sole
purpose of finding recycling bins in close proximity. The address or any other location information
are not stored in any way in any infrastructure by the software. Address information is used for
on-the-fly proximity queries and gets discared after the generation of the response by the skill.

We are the sole owners of the information collected by this skill. 

### your access to and control over information 

You may opt out of any future contacts from us at any time. 
You can do the following at any time by de-listing the Alexa skill from all of your devices.

### security
We take precautions to protect your information. 
When you submit sensitive information via the Alexa service, your information 
is protected both online and offline. 

### updates 

Our Privacy Policy may change from time to time and all updates will be posted on this page. 

If you feel that we are not abiding by this privacy policy, you should contact us immediately via email at hello@versantus.co.uk.

