import uuid
from datetime import datetime

context = {}


def new_request(name, slots={}):
    req_env = {
        "session": {
            "sessionId": "amzn1.echo-api.session.{uuid.uuid1()}",
            "application": {
                "applicationId": "amzn1.echo-sdk-ams.app.{uuid.uuid1()}"
            },
            "user": {
                "userId": "amzn1.ask.account.{uuid.uuid1()}"
            },
            "new": False,
        },
        "request": {
            "requestId": "amzn1.echo-api.request.{uuid.uuid1()}",
            "timestamp": "2018-09-14T09:37:29Z",
            "locale": "en-US",
            "type": "IntentRequest" if name.endswith('Intent') else name,
        }
    }

    # if name.endswith('Intent'):
    #     req_env['request']['intent'] = {
    #         'name': name,
    #         'slots': {k: {'name': k, 'value': v} for k, v in slots.items()}
    #     }

    return req_env


actual_req = {"session": {"new": True, "sessionId": "amzn1.echo-api.session.224e973b-7145-4e3d-8e6f-705816a0d961", "user": {"userId": "amzn1.ask.account.AH762AFVINES7XYFBHGUOGCD53VILXKOPHX3D3CDILDTRIQBYEFLESLWH3JE5RXTQWZ2IJEDRXZTYJR2ZW32SQL5LY5444ZJS2B52DLQ6RIYCXWQJGCF77C4BHHWJZQC6YP2HI33OP24S3LHNJZADA5UFHZQF76M77AIC5WUXPVMDSNVUOU3QI7K2QZXIGXTMBN5G5WPJWKZTPI"}, "application": {"applicationId": "amzn1.ask.skill.35c477e9-a10d-4762-b61f-84019d4caa81"}}, "version": "1.0", "request": {"locale": "en-US", "timestamp": "2018-09-14T09:39:00Z", "dialogState": "STARTED", "intent": {"slots": {"expired": {"name": "expired", "confirmationStatus": "NONE"}, "food_item": {"name": "food_item", "confirmationStatus": "NONE"}}, "name": "getOptionsIntent", "confirmationStatus": "NONE"}, "requestId": "amzn1.echo-api.request.eaf98bf4-9a92-4d61-b5df-060329311d92", "type": "IntentRequest"}, "context": {"Display": {}, "System": {"device": {"deviceId": "amzn1.ask.device.AGJPKE45FO72J6BH2GM3BX2J42RB4X75QELZPCJN6UV5JZFTQ2QWK7QWHKLAQ7BTAD4KYT4JFY5VHW5OLL4YVVSY6XDEUWPOGKOQSGZSF26YBZAH65LQ3IFRLARQEM3LGWUL5HRROCSZXKMQCG3SFCJNZ4W4Y3LQXBT577PJN6MP57P77CT6O", "supportedInterfaces": {"Display": {"templateVersion": "1.0", "markupVersion": "1.0"}}}, "application": {"applicationId": "amzn1.ask.skill.35c477e9-a10d-4762-b61f-84019d4caa81"}, "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjM1YzQ3N2U5LWExMGQtNDc2Mi1iNjFmLTg0MDE5ZDRjYWE4MSIsImV4cCI6MTUzNjkyMTU0MCwiaWF0IjoxNTM2OTE3OTQwLCJuYmYiOjE1MzY5MTc5NDAsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUdKUEtFNDVGTzcySjZCSDJHTTNCWDJKNDJSQjRYNzVRRUxaUENKTjZVVjVKWkZUUTJRV0s3UVdIS0xBUTdCVEFENEtZVDRKRlk1VkhXNU9MTDRZVlZTWTZYREVVV1BPR0tPUVNHWlNGMjZZQlpBSDY1TFEzSUZSTEFSUUVNM0xHV1VMNUhSUk9DU1pYS01RQ0czU0ZDSk5aNFc0WTNMUVhCVDU3N1BKTjZNUDU3UDc3Q1Q2TyIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFINzYyQUZWSU5FUzdYWUZCSEdVT0dDRDUzVklMWEtPUEhYM0QzQ0RJTERUUklRQllFRkxFU0xXSDNKRTVSWFRRV1oySUpFRFJYWlRZSlIyWlczMlNRTDVMWTU0NDRaSlMyQjUyRExRNlJJWUNYV1FKR0NGNzdDNEJISFdKWlFDNllQMkhJMzNPUDI0UzNMSE5KWkFEQTVVRkhaUUY3Nk03N0FJQzVXVVhQVk1EU05WVU9VM1FJN0syUVpYSUdYVE1CTjVHNVdQSldLWlRQSSJ9fQ.WtOzDi_r1MF3aQ4MdeAhd-xynIvnIYfCencvU6qVPb6NXO2UiaLlYGRBehAn4M1or9mTAxP9fDhMoPBDBW_HOj7nIA3YhgD-uuatGtEOF-Yd1v7Unyq2wB0mn_27EQYlR3P05LN2y8liaPX77-VfP_48tx6JZNRKwo-RjULzkQoenCISJEYUyJIDCd-G1X0NtLtBmGdcA5i3WY9XaiXSUqYsZpdiBHMK02rO-eWZmT1G5x9XjwJv4vOrKRQ9HlrB-KVXIOYRHUqX95alvMXS3PLXXRrVdOA5N2P44MCQ42MbLZn9TQbltER-BC5HMojSZIjj99tIwj6N24MVJXQVzg", "user": {"userId": "amzn1.ask.account.AH762AFVINES7XYFBHGUOGCD53VILXKOPHX3D3CDILDTRIQBYEFLESLWH3JE5RXTQWZ2IJEDRXZTYJR2ZW32SQL5LY5444ZJS2B52DLQ6RIYCXWQJGCF77C4BHHWJZQC6YP2HI33OP24S3LHNJZADA5UFHZQF76M77AIC5WUXPVMDSNVUOU3QI7K2QZXIGXTMBN5G5WPJWKZTPI"}, "apiEndpoint": "https://api.eu.amazonalexa.com"}}}