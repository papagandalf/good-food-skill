# -*- coding: utf-8 -*-

import random

import requests
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
from ask_sdk_model.ui import AskForPermissionsConsentCard
from ask_sdk_model.services import ServiceException

from tip_bank import FOOD_WASTE_TIPS, RECYCLING_WASTE_TIPS, USE_BY_TIPS
from database_interaction import find_closest_recycling_bin, find_closest_food_waste_bin, \
    find_closest_food_bank, save_recycle_packaging, save_recycle_food, do_i_recycle_food, do_i_recycle_packaging


def get_location(the_func, handler_input):
    device_address, device_address_status = get_device_address(handler_input)
    if device_address_status in ['no_address_permissions', 'no_address_set']:
        return (None, None), device_address_status
    if device_address is not None:
        device_coordinates = get_device_coordinates(device_address)
        if device_coordinates[0] is not None:
            place, place_coord, _ = the_func(device_coordinates)
            distance = geodesic(device_coordinates, place_coord.replace('POINT', '')).km * 1000
            return (place, round(distance)), 'ok'
        else:
            return (None, None), None
    else:
        return (None, None), None


def get_device_coordinates(address):
    geolocator = Nominatim(user_agent="good_food")
    address_format_no_1 = '{}, {}, {}'.format(address.get('addressLine1', None), address.get('city', None),
                                              address.get('postalCode', None))
    address_format_no_2 = '{}, {}'.format(address.get('addressLine1', None), address.get('postalCode', None))
    address_format_no_3 = '{}'.format(address.get('postalCode', None))

    location = geolocator.geocode(address_format_no_1)
    # if full address does not badge, fall back to address plus postal code
    if location is None:
        location = geolocator.geocode(address_format_no_2)
    # if this does not work either, fall back to just the postal code
    if location is None:
        location = geolocator.geocode(address_format_no_3)
    # otherwise, sorry!
    if location is not None:
        return location.latitude, location.longitude
    else:
        return None, None


def get_device_address(handler_input):
    req_envelope = handler_input.request_envelope
    response_builder = handler_input.response_builder

    if req_envelope.context.system.user.permissions is None or \
            req_envelope.context.system.user.permissions.consent_token is None:
        print(' I am asking to enable permissions')
        return None, 'no_address_permissions'

    try:
        print(' Thinking i can do sth with the address')
        device_id = req_envelope.context.system.device.device_id
        URL = "https://api.eu.amazonalexa.com/v1/devices/{}/settings" \
              "/address".format(req_envelope.context.system.device.device_id)
        TOKEN = req_envelope.context.system.user.permissions.consent_token
        HEADER = {'Accept': 'application/json',
                  'Authorization': 'Bearer {}'.format(TOKEN)}
        addr = requests.get(URL, headers=HEADER).json()
        print(addr)
        if addr.get('postalCode', None) is None and addr.get('city', None):
            response_builder.speak('It looks like you don\'t have an address set. You can set '
                                   'your address from the companion app.')
            print('not set')
            return None, 'no_address_set'
        else:
            return addr, 'ok'
    except ServiceException:
        response_builder.speak('Uh Oh. Looks like something went wrong.')
        return response_builder.response
    except Exception as e:
        raise e


def save_recycling(user_id, kind):
    if kind == 'package':
        save_recycle_packaging(user_id, True)
        speech = 'OK, I will remember that you recycle packaging.'
    elif kind == 'no_package':
        speech = 'OK, I will remember that you do not recycle packaging.'
        save_recycle_packaging(user_id, False)
    elif kind == 'food_waste':
        save_recycle_food(user_id, True)
        speech = 'OK, I will remember that you recycle food waste.'
    elif kind == 'no_food_waste':
        speech = 'OK, I will remember that you do not recycle food waste.'
        save_recycle_food(user_id, False)
    return speech


def dispose_packaging(handler_input):
    if do_i_recycle_packaging(handler_input.request_envelope.context.system.user.user_id):
        return 'Recycle it! You are still recycling mixed packages aren\'t you?', 'ok'
    else:
        (place, distance), location_status = get_location(find_closest_recycling_bin, handler_input)
        if location_status == 'ok':
            response = 'Did you know that you could recycle this package? The closest mixed recycling bin ' \
                                'is only {} away, at {}'.format(distance, place)
        elif location_status == 'no_address_set':
            response = 'I will need your location to give you this information. Please make sure that you have ' \
                       'provided at least a city name or postal code in the Alexa app.'
        elif location_status == 'no_address_permissions':
            response = 'Please enable Location permissions in the Amazon Alexa app.'
        else:
            response = ''
        return response, location_status


def dispose_packaging_specifically(type_of_package, handler_input):
    if do_i_recycle_packaging(handler_input.request_envelope.context.system.user.user_id):
        return 'Recycle it! You are still recycling mixed packages aren\'t you?', 'ok'
    else:
        (place, distance), location_status = get_location(find_closest_recycling_bin, handler_input)
        if location_status == 'ok':
            response = 'Did you know that you could recycle the package? The closest mixed recycling bin is ' \
                                'only {} away, at {}'.format(distance, place)
        elif location_status == 'no_address_set':
            response = 'I will need your location to give you this information. Please make sure that you have ' \
                       'provided at least a city name or postal code in the Alexa app.'
        elif location_status == 'no_address_permissions':
            response = 'Please enable Location permissions in the Amazon Alexa app.'
        else:
            response = ''
        return response, location_status


def dispose_food(handler_input):
    if do_i_recycle_food(handler_input.request_envelope.context.system.user.user_id):
        return 'Recycle it! Good job on recycling food waste!', 'ok'
    else:
        (place, distance), location_status = get_location(find_closest_food_waste_bin, handler_input)
        if location_status == 'ok':
            response = 'Did you know that you could recycle food waste? ' \
                                'The closest food disposal bin is only {} meters away, at {}'.format(distance,
                                                                                              place)
        elif location_status == 'no_address_set':
            response = 'I will need your location to give you this information. Please make sure that you have ' \
                       'provided at least a city name or postal code in the Alexa app.'
        elif location_status == 'no_address_permissions':
            response = 'Please enable Location permissions in the Amazon Alexa app.'
        else:
            response = ''
        return response, location_status


def get_food_recycling_location(handler_input):
    (place, distance), location_status = get_location(find_closest_food_waste_bin, handler_input)
    if location_status == 'ok':
        response = 'The closest food disposal bin is only {} meters away, at {}'.format(distance, place)
    elif location_status == 'no_address_set':
        response = 'I will need your location to give you this information. Please make sure that you have ' \
                   'provided at least a city name or postal code in the Alexa app.'
    elif location_status == 'no_address_permissions':
        response = 'Please enable Location permissions in the Amazon Alexa app.'
    else:
        response = ''
    return response, location_status


def get_packaging_recyclinc_location(handler_input):
    (place, distance), location_status = get_location(find_closest_recycling_bin, handler_input)
    if location_status == 'ok':
        response = 'The closest recycling bin is only {} meters away, at {}. Consider also taking ideas from arts and ' \
                   'crafts websites on how to reuse materials.'.format(distance, place)
    elif location_status == 'no_address_set':
        response = 'I will need your location to give you this information. Please make sure that you have ' \
                   'provided at least a city name or postal code in the Alexa app.'
    elif location_status == 'no_address_permissions':
        response = 'Please enable Location permissions in the Amazon Alexa app.'
    else:
        response = ''
    return response, location_status


def get_package_recycling_tips():
    random_food_tip = random.choice(RECYCLING_WASTE_TIPS)
    return random_food_tip


def get_food_recycling_tips():
    random_recycling_tip = random.choice(FOOD_WASTE_TIPS)
    return random_recycling_tip


def get_use_by_tip():
    random_expiration_date_tip = random.choice(USE_BY_TIPS)
    return random_expiration_date_tip


def get_a_recipe_with(ingredient):
    response = 'You can cook something with this ingredient. You can ask Amazon Echo for recipes with {}'.format(ingredient)
    return response, response


def get_options():
    response = 'Food items can have several uses. Please ask me for more details.'
    return response, response


def get_options(expired):
    if expired:
        response = 'Food items that are no longer safe to eaat can be put in food waste. Ask me for details'
    else:
        response = 'If it can feed more than one person, consider giving it to some charity or food bank nearby. ' \
                   ' Ask me about food banks. Also, several supermarkets within Edinburgh accept food donations. ' \
                   'Just as a reminder they do not accept open packages, fresh or frozen food and alcohol. ' \
                   'Otherwise, you can cook something with it as an ingredient. For either of the two, ask me ' \
                   'for details.'
    return response, response


def get_expiration_dates_tips():
    expiration_dates_tips = {}
    return expiration_dates_tips[0]


def food_bank_position(handler_input):
    (place, _), location_status = get_location(find_closest_food_bank, handler_input)
    if location_status == 'ok':
        response = 'The closest food bank is {}.'.format(place)
        response += ' Also, several supermarkets within Edinburgh accept food donations. Just as a ' \
                    'reminder they do not accept open packages, fresh or frozen food and alcohol.'
    elif location_status == 'no_address_set':
        response = 'I will need your location to give you this information. Please make sure that you have ' \
                   'provided at least a city name or postal code in the Alexa app.'
    elif location_status == 'no_address_permissions':
        response = 'Please enable Location permissions in the Amazon Alexa app.'
    else:
        response = ''
    return response, location_status